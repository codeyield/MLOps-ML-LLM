"""
Объяснение логики:

    - Наш класс обслуживания модели - это простой объект Python, который реализует метод predict(). 
      Обратите внимание, что в коде вашей реальной модели API-интерфейс Seldon Core не требуется. 
      Все, что вам нужно сделать, это создать объект класса с помощью метода pred (), и все готово!
    
    - Метод predict() принимает в качестве входных данных тензор X и список feature_names. 
      Эти типы определены спецификацией protobuf Seldon Core, которую можно найти здесь. 
      В нашем примере службы мы будем использовать только тензорный объект, поскольку наша модель 
      принимает изображения.
    
    - Нам нужно преобразовать массив байтов из объекта X, переданного Seldon Core, в изображение RGB.
    
    - Затем мы меняем местами каналы изображения, чтобы создать окончательное изображение BGR 
      для обработки (подходящее, скажем, для некоторой обработки OpenCV).
    
    - Обратите внимание, что мы можем внедрить загрузку модели при создании экземпляра класса, 
      а затем очень легко выполнить логический вывод с помощью функции predic ().
"""

import io
import logging
import numpy as np
from PIL import Image

logger = logging.getLogger('__mymodel__')

class MyModel(object):

    def __init__(self):
        logger.info("initializing...")
        logger.info("load model here...")
        self._model = None
        logger.info("model has been loaded and initialized...")

    def predict(self, X, features_names):
        """ Seldon Core Prediction API """

        logger.info("predict called...")

        # Use Pillow to convert to an RGB image then reverse channels.
        logger.info('converting tensor to image')

        img = Image.open(io.BytesIO(X)).convert('RGB')
        img = np.array(img)
        img = img[:,:,::-1]

        logger.info("image size = {}".format(img.shape))
        if self._model:
            logger.info("perform inference here...")
        
        # This will serialize the image into a JSON tensor
        logger.info("returning prediction...")
        # Return the original image sent in RGB
        
        return img[:,:,::-1]
