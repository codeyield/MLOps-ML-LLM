# Демонстрационный проект MLOps

Этот репозиторий является 1/2 частью демонстрации pet-проекта MLOps, которая включает модель машинного обучение, получение данных из источников, необходимые зависимости и CI/CD пайплайны.

Другая MLOps-часть проекта в этом репозитории https://gitlab.com/codeyield/mlops-k8s-seldon

## Основные сущности

- Контейнеризованное приложение с ML-моделью предсказания на данных, которые вытягиваются из хранилища

- Контейнеризованное приложение с парсером данных по расписанию, записывающим новые данные в хранилище

- Хранилище данных на базе [MinIO](https://min.io/docs/minio/container/index.html), эмулирующее взаимодействие с Amazon S3

## Тестовая сборка контейнера с ML-моделью

### Исходные файлы:

- [src/models/predict_model.py](src/models/predict_model.py) - инференс модели для Docker/Pod с API-реализацией на Seldon Core

- [src/models/model_testing.py](src/models/model_testing.py) - скрипт для тестирования инференса перед деплоем образа в Registry

- [Dockerfile](Dockerfile) - сборка инференса с Seldon Core Microservice 

- **ВРЕМЕННО ДЛЯ ТЕСТА:** [src/models/MyModel.py](src/models/MyModel.py)

### Сборка образа и запуск контейнера локально

`docker build -t my-model:latest .`

`docker run --rm --name my-model -p 5000:5000 my-model:latest`

*ВНИМАНИЕ! Наблюдаются проблемы с Seldon Core при запуске контейнера в Windows.*

## Сборка образа и пуш в Dockerhub / Gitlab Registry

`docker build -t codeyield/my-model:0.1.1 .`

`docker push codeyield/my-model:0.1.1`

*ВНИМАНИЕ! Как best practice настоятельно рекомендуется **обновлять номер версии вручную** в целях избежания ситуаций, когда CI/CD автоматически разворачивает в прод не прошедший тестирование `containername:latest` с ошибками.*

### Проверка работы API REST Point

```
curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"data": { "ndarray": [[1,2,3,4]]}}' \
        http://localhost:5000/api/v1.0/predictions
```

Ожидаемый ответ:

`{"data":{"names":[],"ndarray":["hello","world"]},"meta":{}}`


---
---

***ВНИМАНИЕ!\
[Первичная структура проекта](docs/init_structure.md) была создана шаблонизатором cookiecutter. Соответствующие файлы, если они не будут использованы в проекте, в дальнейшем следует удалить во избежание захламления репозитория!***
